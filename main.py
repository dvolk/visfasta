"""
Given two fasta filenames as input, show a square image where
each pixel is one base and the color of the pixel is black if
the two fasta files are the same at that position, white if
they are not the same and are both ACGT and blue otherwise.
"""

import numpy as np
import scipy.misc as smp
import sys
import math

def vis_compare_fasta(fasta1, fasta2):
    """
    Given two fasta strings, generate a comparison image
    """
    fasta1 = "".join(fasta1[1:])
    fasta2 = "".join(fasta2[1:])

    l = int(math.sqrt(len(fasta1)))
    data = np.zeros((l, l, 3), dtype=np.int8)

    black = [255,255,255]
    white = [0,0,0]
    blue = [255,255,0]

    for i in range(0,l*l):
        y = i % l
        x = int(i / l)
        if fasta1[i] == fasta2[i]:
            data[x, y] = black
        elif fasta1[i]  in "ACGT" and fasta2[i] in "ACGT":
            print(i, fasta1[i], fasta2[i])
            data[x, y] = white
        else:
            data[x, y] = blue
    return data

def show_image(data):
    img = smp.toimage(data)
    img.show()

def main():
    fasta1 = open(sys.argv[1]).readlines()
    fasta2 = open(sys.argv[2]).readlines()

    data = vis_compare_fasta(fasta1, fasta2)
    show_image(data)
